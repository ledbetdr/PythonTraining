###################################################
#Python Training Companion
#
#This document was created by David Ledbetter
#
#Please read this entire document before you begin
#
#If you have any questions please forward them to:
#   dledbetter@arete.com
#or consult your local python expert
###################################################

Step 1: Download and install Anaconda (Will require administrative privileges)

Go to http://continuum.io/downloads

Select, download, and install the appropriate installer for your operating system

Step 2: Start IPython Notebook

open the command prompt (cmd) or terminal and type:

    ipython notebook --pylab inline

Inside this console the python kernel will be running. Simultaneously an IPython 
    Notebook will open in your web browser (Chrome, Firefox, Opera, or IE 10+).

Step 3: Load the PythonCrashCourse Notebook

From this webpage you can now load any IPython Notebook which you'll find in 
    the included notebook directory. Simply press the 'click here' to load a notebook.
    After selecting the notebook hit the upload button. You can then click on the notebook.
    Alternatively you may navigate at the command prompt/terminal to the folder
    containing the desired notebook and the webpage will auto populate.

Step 4: Follow the tutorial to come up to speed with the Python basics

Note: Don't worry about the 'What You Need to Install' section. We've already covered
         that by installing Anaconda

Step 5: Continue on to any subsequent notebooks that are relevant

Synopsis of the included notebooks (in my biased order):

Pandas Tutorial - Pandas is amazing. After you learn to utilize pandas you'll wonder
                         how you ever lived without it. It creates a container (called a DataFrame)
                         which will hold your data. It will allow you to quickly and easily
                         slice, analyse, impute, and visualize your data. It also makes reading
                         csv's and excel spreadsheets easier than you can believe. 
                         
SKLearn Tutorial - SKLearn is the best, most comprehensive machine learning library
                           available (SKLearn is the primary reason to use python instead of R).
                           The Arete MaLT package is built upon the SKLearn package and an
                           understanding of the SKLearn fundamentals will help you both utilize
                           and extend MaLT.
                           
Matplotlib Tutorial - Matplotlib is one of the primary plotting libraries available for python
                            (Others include Bokeh and ggplot2). Matplotlib, as its name suggests,
                            is essentially an implementation of the Matlab plotting libraries.
                            It should be familiar to those coming from Matlab; Those coming from
                            IDL will need a little extra time to get used to the figure/axis paradigm.
                            The end result will generate much more aesthetically pleasing plots
                            than possible with IDL.
                            
IPython In Depth - IPython (which is the backbone for the IPython console, the IPython
                            QTConsole, and the IPython Notebook) is an extremely powerful
                            command line interpreter. IPython gives access to python, the terminal,
                            and a number of 'magic' commands to facilitate common tasks.
                            
Compressive Sampling - For those interested in compressive sampling

Fourier Analysis - A simple tutorial of using Fourier analysis to learn python                           


Notes on importing packages:

I have one complaint with the pandas tutorial, and that's the usage of:

from pandas import DataFrame

Instead, pandas should be imported in the following manner:

import pandas as pd

This will change the DataFrame construction to become

myDF = pd.DataFrame(fromMatrix) #instead of myDF = DataFrame(fromMatrix)

A similar schema should be utilized for all the major packages. The beginning
    of almost all of my python scripts looks like this:
    
    import numpy as np
    import scipy as sp
    import pandas as pd
    import matplotlib.pyplot as plt
    
In this way when functions are called it's easier to immediately recognize from
    which package the function is being called. This becomes even more important
    when there are similar functions (especially between numpy and scipy) which
    may have slightly different functionality.
    

FAQ

Python 2.7 v. 3.3?

Please use python 2.7. Python 3.3 is a fine language,  but the power of python lies
    in the utility offered by the open source community. This community primarily
    develops for python 2.7. While there are a large number of packages available
    for python 3.3 there are many that are not. On the other hand, there are essentially
    no packages available for python 3.3 which cannot be found for python 2.7.

    
Which IDE should I use?

There are many development environments available for python. However, three of those
    environments are available inside spaces, and as such should be your development
    environments of choice.
    
    1) IPython terminal + Text Editor
    2) IPython Notebook
    3) Spyder
    
    If you love your emacs or vim, I recommend option 1. If you plan to collaborate frequently
    with peers, I recommend option 2 (the tutorial process utilizes option 2 to demonstrate the
    ease of sharing both code and process through notebooks). If you enjoy a [mostly] fully
    fledged IDE I would recommend Spyder. In general you should utilize all 3.
    
    
How do I set up Spyder?

Spyder requires two tweaks in order to expose its true potential. The first is utilizing the IPython
    console instead of the basic python console and the second is changing to a Qt backend to
    facilitate plotting functionality. In order to make these two changes do the following:
    
    1) Tools -> Preferences -> IPython console -> Startup -> Open an IPython console at startup -> OK
    2) Tools -> Preferences -> IPython console -> Graphics -> Backend -> Qt -> OK
    
    Restart spyder and the changes will take effect
  
  
Why Anaconda?

Two reasons:

    1) Anaconda contains a curated selection of the most useful available python packages
    2) Anaconda has been approved for both area 1 and area 3