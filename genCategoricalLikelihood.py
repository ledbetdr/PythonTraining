# -*- coding: utf-8 -*-
"""
@author: David Ledbetter
"""

'''
DRL - frequently we are given categorical features with no obvious factoring
        (obvious factoring: troposphere, stratosphere.., cold, mild, hot)
        (non obvious factoring: desert, jungle, arctic..., sunny, rainy...)
        but we would like to be able to both understand how these features
        interact with our signal and potentially leverage the information
        contained in the feature to aid in exploiting the signature.
        
      to do that we can use pandas to quickly help us group the data by the
        categorical value and compute the likelihood of that feature for 
        whichever particular dependent variable is of interest
'''

#DRL - first we'll want to import the libraries which we think we'll need
#       A good guess would be pandas

#DRL - next we'll want to read in the data of interest using pandas
#       Let's read in the pandasData from lesson two which has a new
#       categorical feture which has been added and is located at:
#           Data/pandasData.csv

#DRL - it's always best practice to have a nice clean dataframe, so let's go
#       ahead and forward fill the Collection column which was messy

#DRL - to begin with let's find the global average TargetProb which we'll
#       need later as well as the total number of events

#DRL - now let's generate the groupDF by grouping our dataframe by the 
#       categorical feature 

#DRL - now we can quickly aggregate any statistics using the built in 
#       aggregation function. Remember every column is available inside
#       the groupDF, so be sure to calculate the statistics for the particular
#       column of interest

#DRL - if we just wanted aggregate statistics we would be done (maybe we would
#       make a nice plot using either matplotlib or the built in pandas
#       plotting functionality). We also might want to regularize the
#       likelihoods based on the frequency of occurence. If we only have 1
#       sample for a particular category we may not believe it as much as
#       if we had 100 samples

#DRL - We can use the size() function to find the number of elements in each
#       group (alternatively try using the value_counts() function on the
#       original dataframe)

#DRL - Now attempt to normalize the occurences by the total number of elements
#       in the dataframe (remember vector operations are automatically broadcast)

#DRL - Let's perform a very simple regularization where we rubber band the 
#       likelihood values to the global mean. We cans imply take the fraction
#       of categorical occurence and multiply it by the categorical likelihood
#       and add that to 1 - the fraction of occurence and multiply that by
#       the global mean (a * catLikelihood) + ((1 - a) * globalLikelihood)

#DRL - Now we have a 'dictionary' of the likelihood values for each category
#       and we'd like that data available in the original dataframe
#
#      We could either use a lambda function or a transformation
#
#      For the lambda function we will 'apply' a function across each element
#       of the original data series (the categorical column) and use the 
#       likelihood dictionary we generated to lookup the appropriate likelihood value   

#DRL - for the transformation we will 'transform' the column of the grouped
#       dataframe to generate the values and then pivot the data back into
#       the original dataframe's shape

#DRL - which method was faster? try to use the %timeit magic ipython command
#       to determine which method was faster so it can be implemented into
#       a high-performance computing system

#DRL - Everyone's favourite aspect of python is it's modularity! 
#       Can we rip out the useful functionality of what we've just done to
#       create a function which we could then add to the Aretepy library?
